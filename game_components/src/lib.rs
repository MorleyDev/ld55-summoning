use std::sync::Arc;

use waef_core::{component, EcsExtension, EcsRegistry};

#[component("game:start_menu_widget")]
pub struct StartMenuWidget {}

#[component("game:player")]
pub struct Player {
    pub speed: f32,
    pub damage_cooldown: f64,
    pub health: f32,
    pub max_health: f32,
}

#[component("game:velocity")]
pub struct Velocity(pub [f32; 2]);

#[component("game:lifespan")]
pub struct Lifespan(pub f64);

#[component("game:attack_1_generator")]
pub struct Attack1Generator {
    pub cooldown: f64,
    pub period: f64,
    pub direction: bool,
    pub generate_both_sides: bool,
}

#[component("game:attack_2_generator")]
pub struct Attack2Generator {
    pub cooldown: f64,
    pub period: f64,
}

#[component("game:attack_3_generator")]
pub struct Attack3Generator {
    pub cooldown: f64,
    pub period: f64,
    pub direction: bool,
    pub generate_both_sides: bool,
}

#[component("game:attack_2_portal_generator")]
pub struct Attack2PortalGenerator {
    pub cooldown: f64,
    pub period: f64,
    pub next_rotation: f32,
}

#[component("game:animated_sprite")]
pub struct AnimatedSprite {
    pub frames: i32,
    pub length: f64,
    pub time: f64,
    pub loops: bool,
}

#[component("game:enemy")]
pub struct Enemy {
    pub health: f32,
    pub stun_cooldown: f64,
    pub attack_power: f32,
}

#[component("game:player_attack")]
pub struct PlayerAttack {
    pub health_damage: f32,
    pub force: f32,
    pub stun_duration: f64,
    pub force_from_player: bool,
}

#[component("game:collision_sensor")]
pub struct CollisionSensor {
    pub origin: [f32; 2],
    pub size: [f32; 2],
}

#[component("game:collision_sensor_tween")]
pub struct CollisionSensorTween {
    pub initial_origin: [f32; 2],
    pub initial_size: [f32; 2],
    pub target_size: [f32; 2],
    pub target_origin: [f32; 2],
    pub length: f64,
    pub timer: f64,
}

#[component("game:ai:disabled")]
pub struct AiDisabled {
    pub lifetime: f64,
}

#[component("game:ai:package_advance_towards_player")]
pub struct AiPackageAdvanceTowardsPlayer {
    pub speed: f32,
}

#[component("game:sprite_flash")]
pub struct SpriteFlash {
    pub timer: f64,
    pub length: f64,
}

#[component("game:healthbar_widget_overlay")]
pub struct HealthbarOverlayWidget {}

#[component("game:healthbar_widget_underlay")]
pub struct HealthbarUnderlayWidget {}

#[component("game:game_widget")]
pub struct GameWidget(pub [f32; 2]);

#[component("game:entity")]
pub struct GameEntity {}

pub fn define_game_code_components(ecs: &Arc<dyn EcsRegistry>) {
    _ = ecs.define_component::<StartMenuWidget>();
    _ = ecs.define_component::<GameEntity>();
    _ = ecs.define_component::<Player>();
    _ = ecs.define_component::<Lifespan>();
    _ = ecs.define_component::<Velocity>();
    _ = ecs.define_component::<Attack1Generator>();
    _ = ecs.define_component::<Attack2PortalGenerator>();
    _ = ecs.define_component::<Attack2Generator>();
    _ = ecs.define_component::<AnimatedSprite>();
    _ = ecs.define_component::<Enemy>();
    _ = ecs.define_component::<PlayerAttack>();
    _ = ecs.define_component::<CollisionSensor>();
    _ = ecs.define_component::<CollisionSensorTween>();
    _ = ecs.define_component::<AiDisabled>();
    _ = ecs.define_component::<AiPackageAdvanceTowardsPlayer>();
    _ = ecs.define_component::<SpriteFlash>();

    _ = ecs.define_component::<HealthbarOverlayWidget>();
    _ = ecs.define_component::<HealthbarUnderlayWidget>();
    _ = ecs.define_component::<GameWidget>();
}
