use std::{f32::consts::PI, sync::Arc};

use game_components::{
    AiPackageAdvanceTowardsPlayer, CollisionSensor, Enemy, GameEntity, Lifespan, Player, Velocity,
};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{builders::EntityBuilder, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::{Sprite, TextureArea};
use waef_world2d::{GlobalTransform, Transform};

use crate::{
    assets::GameAssets,
    messages::{GameOver, GameStarted, GameVictory},
    util::{Noise, Vector2},
};

pub enum SpawningLogic {
    Inactive,
    Active((f64, f64, f64, i32)),
}

pub struct EnemySpawningActor {
    pub assets: GameAssets,
    pub ecs: Arc<dyn EcsRegistry>,
    pub state: SpawningLogic,
    pub tick_count: u32,
    pub total_game_length: f64,
}
impl EnemySpawningActor {
    fn on_tick(&mut self, delta_time: f64) {
        match self.state {
            SpawningLogic::Inactive => (),
            SpawningLogic::Active((
                mut enemy_1_cooldown,
                mut enemy_2_cooldown,
                mut game_length,
                mut events_state,
            )) => {
                game_length += delta_time;
                enemy_1_cooldown -= delta_time;
                enemy_2_cooldown -= delta_time;

                self.ecs.query(|_, enemy: &mut Enemy| {
                    enemy.health -= (0.05 * delta_time) as f32;
                });

                if let Some(player_pos) = self.get_player_position() {
                    self.clean_distant_enemies(player_pos);

                    if enemy_1_cooldown < 0.0 {
                        let game_percentage_complete = game_length / self.total_game_length;
                        enemy_1_cooldown += 3.0 - (game_percentage_complete * 2.0).sqrt();
                        self.tick_count += 1;

                        let random_angle = Noise::noise_f32(self.tick_count, 0) * PI;
                        self.spawn_enemy_1(player_pos, random_angle);
                    }
                    if enemy_2_cooldown < 0.0 {
                        let game_percentage_complete = game_length / self.total_game_length;
                        enemy_2_cooldown += 30.0 - (game_percentage_complete * 2.0).sqrt();
                        self.tick_count += 1;

                        if let Some(player_pos) = self.get_player_position() {
                            let random_angle = Noise::noise_f32(self.tick_count, 1) * PI;
                            self.spawn_enemy_2(player_pos, random_angle);
                        }
                    }
                }

                if game_length >= 60.0 && events_state == 0 {
                    events_state = 1;

                    self.spawn_circle_of_enemy_1(30);
                }

                if game_length >= 120.0 && events_state == 1 {
                    events_state = 2;

                    self.spawn_circle_of_enemy_1(60);
                }

                if game_length >= 175.0 && events_state == 2 {
                    events_state = 3;

                    self.spawn_circle_of_enemy_1(120);
                }

                self.state = SpawningLogic::Active((
                    enemy_1_cooldown,
                    enemy_2_cooldown,
                    game_length,
                    events_state,
                ));
            }
        }
    }

    fn spawn_circle_of_enemy_1(&mut self, count: i32) {
        if let Some(player_pos) = self.get_player_position() {
            (0..count)
                .map(|i| (i - count / 2) as f32 / (count as f32 / 2.0) * PI)
                .for_each(|angle| self.spawn_enemy_1(player_pos, angle))
        }
    }

    fn get_player_position(&mut self) -> Option<[f32; 2]> {
        self.ecs
            .query_map(|_, (_, transform): (&Player, &GlobalTransform)| transform.origin)
            .get(0)
            .copied()
    }

    fn clean_distant_enemies(&mut self, player_pos: [f32; 2]) {
        self.ecs
            .query_choose(|id, (_, transform): (&Enemy, &GlobalTransform)| {
                let diff = [
                    player_pos[0] - transform.origin[0],
                    player_pos[1] - transform.origin[1],
                ];
                if Vector2::magnitude_squared(diff) > 4096.0 {
                    Some(id)
                } else {
                    None
                }
            })
            .into_iter()
            .for_each(|id| _ = self.ecs.destroy_entity(id));
    }

    fn spawn_enemy_1(&mut self, player_pos: [f32; 2], random_angle: f32) {
        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(Lifespan(60.0))
            .with_component(Enemy {
                health: 2.0,
                stun_cooldown: 0.0,
                attack_power: 1.0,
            })
            .with_component(CollisionSensor {
                origin: [0.0, 0.0],
                size: [0.875, 1.0],
            })
            .with_component(AiPackageAdvanceTowardsPlayer { speed: 1.5 })
            .with_component(Transform {
                origin: [
                    player_pos[0] + random_angle.cos() * 16.0,
                    player_pos[1] + random_angle.sin() * 16.0,
                ],
                rotation_rads: 0.0,
            })
            .with_component(Velocity([0.0, 0.0]))
            .with_component(Sprite {
                size: [0.875, 1.0],
                texture_area: TextureArea {
                    left: 0,
                    top: 0,
                    width: 28,
                    height: 32,
                },
                texture_id: self.assets.enemy_1_texture,
                z_index: 20,
                ..Default::default()
            })
            .create(&self.ecs)
    }

    fn spawn_enemy_2(&mut self, player_pos: [f32; 2], random_angle: f32) {
        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(Enemy {
                health: 4.0,
                stun_cooldown: 0.0,
                attack_power: 1.0,
            })
            .with_component(CollisionSensor {
                origin: [0.0, 0.0],
                size: [1.75, 2.0],
            })
            .with_component(AiPackageAdvanceTowardsPlayer { speed: 1.5 })
            .with_component(Transform {
                origin: [
                    player_pos[0] + random_angle.cos() * 16.0,
                    player_pos[1] + random_angle.sin() * 16.0,
                ],
                rotation_rads: 0.0,
            })
            .with_component(Velocity([0.0, 0.0]))
            .with_component(Sprite {
                size: [1.75, 2.0],
                texture_area: TextureArea {
                    left: 0,
                    top: 0,
                    width: 28,
                    height: 32,
                },
                texture_id: self.assets.enemy_1_texture,
                z_index: 20,
                ..Default::default()
            })
            .create(&self.ecs)
    }
}
impl IsDispatcher for EnemySpawningActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            if let Some(message) = AppTick::from_message(message) {
                self.on_tick(message.delta_time);
            }
            ExecutionResult::Processed
        } else if message.name == GameStarted::category_name() {
            self.state = SpawningLogic::Active((3.0, 30.0, 0.0, 0));
            ExecutionResult::Processed
        } else if message.name == GameOver::category_name() {
            self.state = SpawningLogic::Inactive;
            ExecutionResult::Processed
        } else if message.name == GameVictory::category_name() {
            self.state = SpawningLogic::Inactive;
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> waef_core::core::DispatchFilter {
        DispatchFilter::OneOf(
            [
                AppTick::category_name().into(),
                GameStarted::category_name().into(),
                GameOver::category_name().into(),
                GameVictory::category_name().into(),
            ]
            .into(),
        )
    }
}
