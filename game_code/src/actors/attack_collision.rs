use std::sync::{mpsc::Sender, Arc};

use game_components::{
    AiDisabled, CollisionSensor, Enemy, GameEntity, Lifespan, Player, PlayerAttack, SpriteFlash,
    Velocity,
};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{builders::EntityBuilder, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::{Sprite, TextureArea};
use waef_kira::messages::PlaySound;
use waef_world2d::{GlobalTransform, Transform};

use crate::{
    assets::GameAssets,
    util::{Geometry, Vector2},
};

pub struct AttackCollision {
    pub dispatcher: Sender<Message>,
    pub ecs: Arc<dyn EcsRegistry>,
    pub assets: GameAssets,
}
impl AttackCollision {
    fn on_tick(&self, delta_time: f64) {
        if let Some(player_coords) = self
            .ecs
            .query_map(|_, (_, transform): (&Player, &GlobalTransform)| transform.origin)
            .get(0)
        {
            let mut collisions = Vec::with_capacity(32);

            self.ecs.query(
                |lhs,
                 (enemy, lhs_transform, lhs_sensor): (
                    &mut Enemy,
                    &GlobalTransform,
                    &CollisionSensor,
                )| {
                    if enemy.stun_cooldown > 0.0 {
                        enemy.stun_cooldown = f64::max(enemy.stun_cooldown - delta_time, 0.0);
                        return;
                    }

                    let lhs_hsize = [lhs_sensor.size[0] / 2.0, lhs_sensor.size[1] / 2.0];
                    let lhs_origin = [
                        lhs_sensor.origin[0] + lhs_transform.origin[0],
                        lhs_sensor.origin[1] + lhs_transform.origin[1],
                    ];
                    let lhs_tlbr = [
                        lhs_origin[0] - lhs_hsize[0],
                        lhs_origin[1] - lhs_hsize[1],
                        lhs_origin[0] + lhs_hsize[0],
                        lhs_origin[1] + lhs_hsize[1],
                    ];

                    self.ecs.query(
                        |rhs,
                         (attack, rhs_transform, rhs_sensor): (
                            &PlayerAttack,
                            &GlobalTransform,
                            &CollisionSensor,
                        )| {
                            if lhs != rhs {
                                let rhs_hsize =
                                    [rhs_sensor.size[0] / 2.0, rhs_sensor.size[1] / 2.0];
                                let rhs_origin = [
                                    rhs_sensor.origin[0] + rhs_transform.origin[0],
                                    rhs_sensor.origin[1] + rhs_transform.origin[1],
                                ];
                                let rhs_tlbr = [
                                    rhs_origin[0] - rhs_hsize[0],
                                    rhs_origin[1] - rhs_hsize[1],
                                    rhs_origin[0] + rhs_hsize[0],
                                    rhs_origin[1] + rhs_hsize[1],
                                ];

                                if Geometry::aabb(lhs_tlbr, rhs_tlbr) {
                                    let attack_force = if attack.force_from_player {
                                        Vector2::mult(
                                            Vector2::normalize([
                                                lhs_transform.origin[0] - player_coords[0],
                                                lhs_transform.origin[1] - player_coords[1],
                                            ]),
                                            attack.force,
                                        )
                                    } else {
                                        Vector2::mult(
                                            Vector2::normalize([
                                                lhs_transform.origin[0] - rhs_transform.origin[0],
                                                lhs_transform.origin[1] - rhs_transform.origin[1],
                                            ]),
                                            attack.force,
                                        )
                                    };
                                    collisions.push((
                                        lhs,
                                        attack.health_damage,
                                        attack_force,
                                        attack.stun_duration,
                                    ));
                                }
                            }
                        },
                    )
                },
            );
            collisions.into_iter().for_each(
                |(enemy, attack_power, attack_force, stun_duration)| {
                    let mut remove_enemy = None;
                    let mut apply_force = None;
                    self.ecs.query_single(enemy, |id, enemy: &mut Enemy| {
                        enemy.health -= attack_power;
                        if enemy.health <= 0.0 {
                            remove_enemy = Some(id);
                        } else {
                            enemy.stun_cooldown = stun_duration;
                            apply_force = Some((id, attack_force, stun_duration))
                        }
                    });
                    if let Some(enemy_id) = remove_enemy {
                        _ = self.dispatcher.send(
                            PlaySound {
                                loops: false,
                                sound_buffer_id: self.assets.enemy_1_death_sound,
                                sound_id: uuid::Uuid::new_v4().as_u128(),
                                volume: 0.25,
                            }
                            .into_message(),
                        );

                        let mut result = None;
                        self.ecs.query_single(
                            enemy_id,
                            |_, (sprite, transform): (&Sprite, &GlobalTransform)| {
                                result = Some((
                                    Sprite {
                                        texture_id: sprite.texture_id,
                                        texture_area: TextureArea {
                                            left: 0,
                                            top: sprite.texture_area.height * 2,
                                            width: sprite.texture_area.width,
                                            height: sprite.texture_area.height,
                                        },
                                        size: sprite.size,
                                        z_index: 20,
                                        ..Default::default()
                                    },
                                    transform.clone(),
                                ));
                            },
                        );
                        _ = self.ecs.destroy_entity(enemy_id);

                        if let Some((sprite, transform)) = result {
                            _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                                .with_component(GameEntity {})
                                .with_component(Lifespan(1.0))
                                .with_component(Transform {
                                    origin: transform.origin,
                                    rotation_rads: transform.rotation_rads,
                                })
                                .with_component(sprite)
                                .create(&self.ecs);
                        }
                    }
                    if let Some((enemy_id, force, stun_duration)) = apply_force {
                        _ = self.dispatcher.send(
                            PlaySound {
                                loops: false,
                                sound_buffer_id: self.assets.enemy_1_hit_sound,
                                sound_id: uuid::Uuid::new_v4().as_u128(),
                                volume: 0.25,
                            }
                            .into_message(),
                        );

                        if stun_duration > 0.0 {
                            _ = self.ecs.attach_component(
                                enemy_id,
                                AiDisabled {
                                    lifetime: stun_duration,
                                },
                            );

                            _ = self.ecs.attach_component(
                                enemy_id,
                                SpriteFlash {
                                    length: stun_duration,
                                    timer: stun_duration,
                                },
                            );
                        }

                        if force[0] > 0.0 || force[1] > 0.0 {
                            self.ecs.query_single(
                                enemy_id,
                                |_, Velocity(velocity): &mut Velocity| {
                                    *velocity = force;
                                },
                            );
                        }
                    }
                },
            );
        }
    }
}
impl IsDispatcher for AttackCollision {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            if let Some(message) = AppTick::from_message(message) {
                self.on_tick(message.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
