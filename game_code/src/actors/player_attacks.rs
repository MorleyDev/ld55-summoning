use std::{
    f32::consts::PI,
    sync::{mpsc::Sender, Arc},
};

use crate::assets::GameAssets;
use game_components::{
    AnimatedSprite, Attack1Generator, Attack2Generator, Attack2PortalGenerator, Attack3Generator,
    CollisionSensor, CollisionSensorTween, GameEntity, Lifespan, PlayerAttack,
};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{builders::EntityBuilder, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::{Sprite, TextureArea};
use waef_kira::messages::PlaySound;
use waef_world2d::{GlobalTransform, Parent, Transform};

pub struct PlayerAttacksActor {
    pub ecs: Arc<dyn EcsRegistry>,
    pub dispatcher: Sender<Message>,
    pub assets: GameAssets,
    pub tick_count: u32,
}
impl PlayerAttacksActor {
    fn on_tick(&mut self, delta_time: f64) {
        self.run_attack_1_generator(delta_time);
        self.run_attack_2_portal_generator(delta_time);
        self.run_attack_2_generator(delta_time);
        self.run_attack_3_generator(delta_time);
        self.tick_count += 1;
    }

    fn run_attack_2_portal_generator(&self, delta_time: f64) {
        self.ecs
            .query_choose(
                |_, (attack, transform): (&mut Attack2PortalGenerator, &GlobalTransform)| {
                    attack.cooldown -= delta_time;
                    if attack.cooldown > 0.0 {
                        None
                    } else {
                        let rotation = attack.next_rotation;
                        attack.next_rotation -= 0.2;
                        attack.cooldown += attack.period;
                        Some((transform.origin, rotation))
                    }
                },
            )
            .into_iter()
            .for_each(|(origin, angle)| {
                let portal_angle = angle * PI;
                let portal_distance = 2.5;

                _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                    .with_component(GameEntity {})
                    .with_component(Lifespan(2.5))
                    .with_component(Attack2Generator {
                        cooldown: 0.5,
                        period: 5.0,
                    })
                    .with_component(Transform {
                        origin: [
                            origin[0] + portal_angle.cos() * portal_distance,
                            origin[1] + portal_angle.sin() * portal_distance,
                        ],
                        rotation_rads: 0.0,
                    })
                    .with_component(Sprite {
                        size: [2.0, 2.0],
                        texture_area: TextureArea {
                            left: 0,
                            top: 0,
                            width: 32,
                            height: 32,
                        },
                        texture_id: self.assets.attack_2_portal_texture,
                        z_index: 1,
                        ..Default::default()
                    })
                    .create(&self.ecs);
            })
    }

    fn run_attack_2_generator(&self, delta_time: f64) {
        self.ecs
            .query_choose(
                |_, (attack, transform): (&mut Attack2Generator, &GlobalTransform)| {
                    if attack.cooldown <= 0.0 {
                        None
                    } else {
                        attack.cooldown -= delta_time;
                        if attack.cooldown > 0.0 {
                            None
                        } else {
                            attack.cooldown += attack.period;
                            Some(transform.origin)
                        }
                    }
                },
            )
            .into_iter()
            .for_each(|origin| {
                _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                    .with_component(GameEntity {})
                    .with_component(Lifespan(2.0))
                    .with_component(PlayerAttack {
                        health_damage: 1.5,
                        force: 5.0,
                        stun_duration: 1.0,
                        force_from_player: false,
                    })
                    .with_component(CollisionSensor {
                        origin: [0.0, 0.0],
                        size: [0.2, 0.2],
                    })
                    .with_component(CollisionSensorTween {
                        initial_origin: [0.0, 0.0],
                        initial_size: [0.2, 0.2],
                        target_size: [2.5, 2.5],
                        target_origin: [0.0, 0.0],
                        length: 0.5,
                        timer: 0.0,
                    })
                    .with_component(Transform {
                        origin: origin,
                        rotation_rads: 0.0,
                    })
                    .with_component(AnimatedSprite {
                        frames: 4,
                        length: 0.5,
                        loops: false,
                        time: 0.0,
                    })
                    .with_component(Sprite {
                        size: [2.5, 2.5],
                        texture_area: TextureArea {
                            top: 0,
                            left: 0,
                            width: 32,
                            height: 32,
                        },
                        texture_id: self.assets.attack_2_texture,
                        z_index: 2,
                        ..Default::default()
                    })
                    .create(&self.ecs)
            });
    }

    fn run_attack_1_generator(&self, delta_time: f64) {
        // run the attack generator
        self.ecs
            .query_choose(|id, attack: &mut Attack1Generator| {
                attack.cooldown -= delta_time;
                if attack.cooldown > 0.0 {
                    None
                } else {
                    let direction = attack.direction;
                    attack.cooldown += attack.period;
                    attack.direction = !attack.direction;
                    Some((id, direction, attack.generate_both_sides))
                }
            })
            .into_iter()
            .for_each(|(parent_id, direction, generate_on_both_sides)| {
                _ = self.dispatcher.send(
                    PlaySound {
                        loops: false,
                        sound_buffer_id: self.assets.attack_1_sound,
                        sound_id: uuid::Uuid::new_v4().as_u128(),
                        volume: 0.1,
                    }
                    .into_message(),
                );

                // create the attacking block
                if generate_on_both_sides {
                    self.spawn_attack_1(parent_id, true);
                    self.spawn_attack_1(parent_id, false);
                } else {
                    self.spawn_attack_1(parent_id, direction);
                }
            });
    }

    fn run_attack_3_generator(&self, delta_time: f64) {
        // run the attack generator
        self.ecs
            .query_choose(|id, attack: &mut Attack3Generator| {
                attack.cooldown -= delta_time;
                if attack.cooldown > 0.0 {
                    None
                } else {
                    let direction = attack.direction;
                    attack.cooldown += attack.period;
                    attack.direction = !attack.direction;
                    Some((id, direction, attack.generate_both_sides))
                }
            })
            .into_iter()
            .for_each(|(parent_id, direction, generate_both_sides)| {
                _ = self.dispatcher.send(
                    PlaySound {
                        loops: false,
                        sound_buffer_id: self.assets.attack_1_sound,
                        sound_id: uuid::Uuid::new_v4().as_u128(),
                        volume: 0.1,
                    }
                    .into_message(),
                );

                // create the attacking block
                if generate_both_sides {
                    self.spawn_attack_3(parent_id, true);
                    self.spawn_attack_3(parent_id, false);
                } else {
                    self.spawn_attack_3(parent_id, direction);
                }
            });
    }

    fn spawn_attack_1(&self, parent_id: u128, direction: bool) {
        let direction_factor = if direction { 1.0 } else { -1.0 };
        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(Lifespan(0.25))
            .with_component(PlayerAttack {
                health_damage: 1.0,
                force: 2.0,
                stun_duration: 0.5,
                force_from_player: true,
            })
            .with_component(CollisionSensor {
                origin: [0.0, 0.0],
                size: [1.0, 1.0],
            })
            .with_component(CollisionSensorTween {
                initial_origin: [direction_factor * -0.3333, 0.1],
                initial_size: [0.0, 0.0],
                target_size: [1.0, 0.5],
                target_origin: [0.0, 0.1],
                length: 0.2,
                timer: 0.0,
            })
            .with_component(Parent {
                entity_id: parent_id,
            })
            .with_component(Transform {
                origin: [direction_factor * 1.25, 0.0],
                rotation_rads: 0.0,
            })
            .with_component(AnimatedSprite {
                frames: 4,
                length: 0.2,
                loops: false,
                time: 0.0,
            })
            .with_component(Sprite {
                size: [1.0, 1.0],
                texture_area: TextureArea {
                    top: if direction { 0 } else { 32 },
                    left: 0,
                    width: 32,
                    height: 32,
                },
                texture_id: self.assets.attack_1_texture,
                z_index: 3,
                ..Default::default()
            })
            .create(&self.ecs)
    }

    fn spawn_attack_3(&self, parent_id: u128, direction: bool) {
        let direction_factor = if direction { 1.0 } else { -1.0 };
        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(Lifespan(0.25))
            .with_component(PlayerAttack {
                health_damage: 2.0,
                force: 5.0,
                stun_duration: 1.0,
                force_from_player: true,
            })
            .with_component(CollisionSensor {
                origin: [0.0, 0.0],
                size: [1.0, 1.0],
            })
            .with_component(CollisionSensorTween {
                initial_origin: [0.0, direction_factor * -0.3333],
                initial_size: [0.0, 0.0],
                target_size: [0.5, 1.0],
                target_origin: [0.0, 0.1],
                length: 0.15,
                timer: 0.0,
            })
            .with_component(Parent {
                entity_id: parent_id,
            })
            .with_component(Transform {
                origin: [0.0, direction_factor * 1.25],
                rotation_rads: 0.0,
            })
            .with_component(AnimatedSprite {
                frames: 4,
                length: 0.15,
                loops: false,
                time: 0.0,
            })
            .with_component(Sprite {
                size: [0.875, 1.0],
                texture_area: TextureArea {
                    top: if direction { 0 } else { 32 },
                    left: 0,
                    width: 24,
                    height: 32,
                },
                texture_id: self.assets.attack_3_texture,
                z_index: 3,
                ..Default::default()
            })
            .create(&self.ecs)
    }
}
impl IsDispatcher for PlayerAttacksActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == "app:tick" {
            if let Some(msg) = AppTick::from_message(message) {
                self.on_tick(msg.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
