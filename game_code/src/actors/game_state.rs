use std::sync::{mpsc::Sender, Arc};

use crate::messages::{GameReady, GameStarted, GameVictory};
use crate::{assets::GameAssets, messages::GameOver};
use game_components::{
    Attack1Generator, CollisionSensor, Enemy, GameEntity, GameWidget, HealthbarOverlayWidget,
    HealthbarUnderlayWidget, Player, PlayerAttack, StartMenuWidget, Velocity,
};
use waef_controls::{
    events::{ControlAxisEvent, ControlButtonEvent},
    system::window::OnWindowCloseRequested,
};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{builders::EntityBuilder, extensions::EcsExtension, EcsRegistry},
};
use waef_graphics2d::ecs::{Camera, Sprite, TextureArea};
use waef_kira::messages::{PlaySound, StopSound};
use waef_world2d::{GlobalTransform, Transform};

#[derive(PartialEq)]
pub enum GameState {
    Waiting,
    Ready,
    Playing,
    GameOver,
    Victory,
}

pub struct GameStateActor {
    pub dispatcher: Sender<Message>,
    pub ecs: Arc<dyn EcsRegistry>,
    pub assets: GameAssets,
    pub game_state: GameState,
}
impl GameStateActor {
    pub fn on_ready(&mut self) {
        self.game_state = GameState::Ready;

        _ = self.dispatcher.send(
            PlaySound {
                loops: true,
                sound_buffer_id: self.assets.title_music,
                sound_id: 1,
                volume: 0.2,
            }
            .into_message(),
        );

        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(StartMenuWidget {})
            .with_component(GameWidget([0.0, 0.0]))
            .with_component(Transform {
                origin: [0.0, 0.0],
                rotation_rads: 0.0,
            })
            .with_component(Camera { zoom: 1.0 })
            .with_component(Sprite {
                texture_id: self.assets.title_texture,
                texture_area: TextureArea {
                    left: 0,
                    top: 0,
                    width: 512,
                    height: 512,
                },
                size: [1.0, 1.0],
                z_index: 100,
                ..Default::default()
            })
            .create(&self.ecs);
    }

    fn on_start(&mut self) {
        self.game_state = GameState::Playing;

        _ = self
            .dispatcher
            .send(StopSound { sound_id: 1 }.into_message());

        self.ecs
            .query_map(|id, _: &StartMenuWidget| id)
            .into_iter()
            .for_each(|id| _ = self.ecs.destroy_entity(id));

        _ = self.dispatcher.send(
            PlaySound {
                loops: true,
                sound_id: 1,
                sound_buffer_id: self.assets.game_music,
                volume: 0.1,
            }
            .into_message(),
        );

        let player_id = uuid::Uuid::new_v4().as_u128();

        // create player + camera entity
        _ = EntityBuilder::new(player_id)
            .with_component(GameEntity {})
            .with_component(Player {
                speed: 4.0,
                damage_cooldown: 0.0,
                health: 5.0,
                max_health: 5.0,
            })
            .with_component(Velocity([0.0, 0.0]))
            .with_component(Camera { zoom: 16.0 })
            .with_component(CollisionSensor {
                origin: [0.0, 0.0],
                size: [0.5, 0.75],
            })
            .with_component(Attack1Generator {
                cooldown: 2.0,
                period: 2.0,
                direction: true,
                generate_both_sides: false,
            })
            .with_component(Transform {
                origin: [0.0, 0.0],
                rotation_rads: 0.0,
            })
            .with_component(Sprite {
                size: [0.875, 1.0],
                texture_area: TextureArea {
                    top: 0,
                    left: 0,
                    width: 28,
                    height: 32,
                },
                texture_id: self.assets.player_texture,
                z_index: 10,
                ..Default::default()
            })
            .create(&self.ecs);

        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(HealthbarOverlayWidget {})
            .with_component(GameWidget([0.0, 7.5]))
            .with_component(Transform {
                origin: [0.0, 7.5],
                rotation_rads: 0.0,
            })
            .with_component(Sprite {
                size: [15.0, 0.75],
                texture_area: TextureArea {
                    left: 0,
                    top: 0,
                    width: 512,
                    height: 24,
                },
                z_index: 101,
                texture_id: self.assets.healthbar_overlay_texture,
                ..Default::default()
            })
            .create(&self.ecs);

        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(GameWidget([0.0, 7.5]))
            .with_component(HealthbarUnderlayWidget {})
            .with_component(Transform {
                origin: [0.0, 7.5],
                rotation_rads: 0.0,
            })
            .with_component(Sprite {
                size: [15.0, 0.75],
                texture_area: TextureArea {
                    left: 0,
                    top: 0,
                    width: 512,
                    height: 24,
                },
                z_index: 100,
                texture_id: self.assets.healthbar_underlay_texture,
                ..Default::default()
            })
            .create(&self.ecs);
    }

    fn on_player_move(&self, axis: ControlAxisEvent) {
        self.ecs.query(
            |_id, (player, Velocity(velocity)): (&Player, &mut Velocity)| {
                velocity[0] = axis.x * player.speed;
                velocity[1] = axis.y * player.speed;
            },
        );
        self.ecs
            .query(|_id, (_player, sprite): (&Player, &mut Sprite)| {
                if axis.x > 0.0 {
                    sprite.texture_area.top = 32;
                } else if axis.x < 0.0 {
                    sprite.texture_area.top = 0;
                }
            });
    }

    fn on_game_over(&mut self) {
        self.game_state = GameState::GameOver;

        _ = self
            .dispatcher
            .send(StopSound { sound_id: 1 }.into_message());

        _ = self.dispatcher.send(
            PlaySound {
                loops: true,
                sound_buffer_id: self.assets.gameover_music,
                sound_id: 1,
                volume: 0.2,
            }
            .into_message(),
        );
    }

    fn on_restart(&mut self) {
        self.ecs
            .query_map(|id, _: &GameEntity| id)
            .into_iter()
            .for_each(|id| _ = self.ecs.destroy_entity(id));

        self.game_state = GameState::Ready;

        _ = self.dispatcher.send(GameStarted {}.into_message());
    }

    fn on_game_victory(&mut self) {
        self.game_state = GameState::Victory;

        _ = self
            .dispatcher
            .send(StopSound { sound_id: 1 }.into_message());

        _ = self.dispatcher.send(
            PlaySound {
                loops: false,
                sound_buffer_id: self.assets.victory_sound,
                sound_id: uuid::Uuid::new_v4().as_u128(),
                volume: 0.2,
            }
            .into_message(),
        );

        _ = self.dispatcher.send(
            PlaySound {
                loops: false,
                sound_buffer_id: self.assets.victory_music,
                sound_id: 1,
                volume: 0.2,
            }
            .into_message(),
        );

        self.ecs
            .query_map(|id, _: &PlayerAttack| id)
            .into_iter()
            .for_each(|id| _ = self.ecs.destroy_entity(id));

        let player_coords = self
            .ecs
            .query_map(|_, (_, transform): (&Player, &GlobalTransform)| transform.origin)
            .get(0)
            .cloned()
            .unwrap_or([0.0, 0.0]);

        _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
            .with_component(GameEntity {})
            .with_component(Transform {
                origin: player_coords,
                rotation_rads: 0.0,
            })
            .with_component(Sprite {
                size: [16.0, 16.0],
                texture_area: TextureArea {
                    left: 0,
                    top: 0,
                    width: 512,
                    height: 512,
                },
                texture_id: self.assets.victory_texture,
                z_index: 666,
                color: [1.0, 1.0, 1.0, 0.25],
                ..Default::default()
            })
            .create(&self.ecs);

        self.ecs
            .query_map(|id, _: &HealthbarOverlayWidget| id)
            .into_iter()
            .for_each(|hw| _ = self.ecs.destroy_entity(hw));
        self.ecs
            .query_map(|id, _: &HealthbarUnderlayWidget| id)
            .into_iter()
            .for_each(|hw| _ = self.ecs.destroy_entity(hw));

        self.ecs
            .query_map(
                |id, (_, transform, sprite): (&Player, &GlobalTransform, &Sprite)| {
                    (id, transform.clone(), sprite.clone())
                },
            )
            .into_iter()
            .for_each(|(dead_player_id, transform, sprite)| {
                _ = self.ecs.destroy_entity(dead_player_id);

                _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                    .with_component(GameEntity {})
                    .with_component(Camera { zoom: 16.0 })
                    .with_component(Transform {
                        origin: transform.origin,
                        rotation_rads: transform.rotation_rads,
                    })
                    .with_component(Sprite {
                        color: [1.0, 1.0, 1.0, 1.0],
                        texture_area: TextureArea {
                            left: 0,
                            top: sprite.texture_area.height * 2,
                            ..sprite.texture_area
                        },
                        ..sprite
                    })
                    .create(&self.ecs);
            });

        self.ecs
            .query_map(
                |id, (_, transform, sprite): (&Enemy, &GlobalTransform, &Sprite)| {
                    (id, transform.clone(), sprite.clone())
                },
            )
            .into_iter()
            .for_each(|(dead_enemy_id, transform, sprite)| {
                _ = self.ecs.destroy_entity(dead_enemy_id);

                _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                    .with_component(GameEntity {})
                    .with_component(Transform {
                        origin: transform.origin,
                        rotation_rads: transform.rotation_rads,
                    })
                    .with_component(Sprite {
                        color: [1.0, 1.0, 1.0, 1.0],
                        texture_area: TextureArea {
                            left: 0,
                            top: sprite.texture_area.height * 2,
                            ..sprite.texture_area
                        },
                        ..sprite
                    })
                    .create(&self.ecs);
            });
    }
}
impl IsDispatcher for GameStateActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == "controls:axis:player_move" {
            if let Some(msg) = ControlAxisEvent::from_message(message) {
                self.on_player_move(msg);
            }
            ExecutionResult::Processed
        } else if message.name == "controls:button:start_game:pressed" {
            match self.game_state {
                GameState::Waiting => (),
                GameState::Ready => _ = self.dispatcher.send(GameStarted {}.into_message()),
                GameState::Playing => (),
                GameState::GameOver => self.on_restart(),
                GameState::Victory => (),
            }

            ExecutionResult::Processed
        } else if message.name == OnWindowCloseRequested::category_name() {
            ExecutionResult::Kill
        } else if message.name == GameReady::category_name() {
            self.on_ready();
            ExecutionResult::Processed
        } else if message.name == GameOver::category_name() {
            self.on_game_over();
            ExecutionResult::Processed
        } else if message.name == GameVictory::category_name() {
            self.on_game_victory();
            ExecutionResult::Processed
        } else if message.name == GameStarted::category_name() {
            self.on_start();
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(
            [
                OnWindowCloseRequested::category_name().into(),
                GameReady::category_name().into(),
                GameStarted::category_name().into(),
                GameOver::category_name().into(),
                GameVictory::category_name().into(),
                ControlButtonEvent::category_name().into(),
                ControlAxisEvent::category_name().into(),
            ]
            .into(),
        )
    }
}
