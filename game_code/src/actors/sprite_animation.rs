use std::sync::Arc;

use game_components::AnimatedSprite;
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::Sprite;

pub struct GameAnimationActor {
    pub ecs: Arc<dyn EcsRegistry>,
}
impl GameAnimationActor {
    fn on_tick(&self, delta_time: f64) {
        self.ecs.query(
            |_id, (sprite, animation): (&mut Sprite, &mut AnimatedSprite)| {
                animation.time += delta_time;
                if animation.time >= animation.length {
                    if animation.loops {
                        while animation.time >= animation.length {
                            animation.time -= animation.length;
                        }
                    } else {
                        sprite.texture_area.left =
                            (animation.frames - 1) * sprite.texture_area.width;
                        return;
                    }
                }
                sprite.texture_area.left = sprite.texture_area.width
                    * ((animation.frames as f64) * (animation.time / animation.length)) as i32;
            },
        );
    }
}
impl IsDispatcher for GameAnimationActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == "app:tick" {
            if let Some(msg) = AppTick::from_message(message) {
                self.on_tick(msg.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
