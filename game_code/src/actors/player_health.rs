use std::sync::{mpsc::Sender, Arc};

use game_components::{GameEntity, GameWidget, HealthbarUnderlayWidget, Player};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{builders::EntityBuilder, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::{Camera, Sprite, TextureArea};
use waef_kira::messages::PlaySound;
use waef_world2d::{GlobalTransform, Transform};

use crate::{assets::GameAssets, messages::GameOver};

pub struct PlayerHealthActor {
    pub ecs: Arc<dyn EcsRegistry>,
    pub dispatcher: Sender<Message>,
    pub assets: GameAssets,
}
impl PlayerHealthActor {
    fn on_tick(&self) {
        self.ecs
            .query_map(|_, (player, transform): (&Player, &GlobalTransform)| {
                (player.health / player.max_health, transform.origin)
            })
            .into_iter()
            .for_each(|(pctg, origin)| {
                self.ecs.query(
                    |_, (transform, GameWidget(widget)): (&mut Transform, &GameWidget)| {
                        transform.origin = [origin[0] + widget[0], origin[1] + widget[1]];
                    },
                );

                if pctg == 0.0 {
                    self.ecs
                        .query_map(|id, _: &HealthbarUnderlayWidget| id)
                        .into_iter()
                        .for_each(|id| _ = self.ecs.destroy_entity(id));
                } else {
                    self.ecs.query(
                        |_,
                         (underlay_sprite, transform, _): (
                            &mut Sprite,
                            &mut Transform,
                            &HealthbarUnderlayWidget,
                        )| {
                            underlay_sprite.size[0] = 15.0 * pctg;
                            transform.origin = [origin[0], origin[1] + 7.5];
                        },
                    );
                }
            });

        self.ecs
            .query_choose(
                |id, (player, transform, sprite): (&Player, &GlobalTransform, &Sprite)| {
                    if player.health <= 0.0 {
                        Some((id, transform.clone(), sprite.clone()))
                    } else {
                        None
                    }
                },
            )
            .into_iter()
            .for_each(|(dead_player_id, transform, sprite)| {
                _ = self.dispatcher.send(GameOver {}.into_message());

                _ = self.dispatcher.send(
                    PlaySound {
                        loops: false,
                        sound_buffer_id: self.assets.player_death_sound,
                        sound_id: uuid::Uuid::new_v4().as_u128(),
                        volume: 0.2,
                    }
                    .into_message(),
                );

                _ = self.ecs.destroy_entity(dead_player_id);

                _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                    .with_component(GameEntity {})
                    .with_component(Camera { zoom: 16.0 })
                    .with_component(Transform {
                        origin: transform.origin,
                        rotation_rads: transform.rotation_rads,
                    })
                    .with_component(Sprite {
                        color: [1.0, 1.0, 1.0, 1.0],
                        texture_area: TextureArea {
                            left: 0,
                            top: sprite.texture_area.height * 2,
                            ..sprite.texture_area
                        },
                        ..sprite
                    })
                    .create(&self.ecs);
            });
    }
}
impl IsDispatcher for PlayerHealthActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            self.on_tick();
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
