use std::{
    collections::HashMap,
    sync::{mpsc::Sender, Arc},
};

use game_components::Player;
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::messages::{AttachTileMap, DetachTileMap, MountTileSet};
use waef_world2d::GlobalTransform;

use crate::{
    assets::GameAssets,
    messages::{GameReady, GameStarted},
};

pub struct TilemapGenerationActor {
    pub ecs: Arc<dyn EcsRegistry>,
    pub dispatcher: Sender<Message>,
    pub assets: GameAssets,
    pub active_tilemaps: HashMap<(i32, i32), u128>,
}
impl TilemapGenerationActor {
    fn on_tick(&mut self) {
        self.ecs
            .query_map(|_, (_, transform): (&Player, &GlobalTransform)| transform.origin)
            .into_iter()
            .for_each(|player_position| {
                let player_tilemap_position = (
                    (player_position[0] / 32.0) as i32,
                    (player_position[1] / 16.0) as i32,
                );

                [
                    (player_tilemap_position.0 - 1, player_tilemap_position.1 - 1),
                    (player_tilemap_position.0, player_tilemap_position.1 - 1),
                    (player_tilemap_position.0 + 1, player_tilemap_position.1 - 1),
                    (player_tilemap_position.0 - 1, player_tilemap_position.1),
                    (player_tilemap_position.0, player_tilemap_position.1),
                    (player_tilemap_position.0 + 1, player_tilemap_position.1),
                    (player_tilemap_position.0 - 1, player_tilemap_position.1 + 1),
                    (player_tilemap_position.0, player_tilemap_position.1 + 1),
                    (player_tilemap_position.0 + 1, player_tilemap_position.1 + 1),
                ]
                .into_iter()
                .for_each(|(i, j)| {
                    if !self.active_tilemaps.contains_key(&(i, j)) {
                        let tilemap_id = uuid::Uuid::new_v4().as_u128();

                        self.active_tilemaps.insert((i, j), tilemap_id);
                        _ = self.dispatcher.send(
                            AttachTileMap {
                                tilemap_id,
                                tileset_id: self.assets.background_tileset,
                                map_world_origin: [i as f32 * 32.0, j as f32 * 16.0],
                                map_world_size: [32.0, 16.0],
                                tiles_horiz: 4,
                                tiles: vec![1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                                z_index: 0,
                            }
                            .into_message(),
                        );
                    }
                });
            })
    }
}
impl IsDispatcher for TilemapGenerationActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            self.on_tick();
            ExecutionResult::Processed
        } else if message.name == GameReady::category_name() {
            // mount the tileset for the background
            _ = self.dispatcher.send(
                MountTileSet {
                    tileset_id: self.assets.background_tileset,
                    texture_id: self.assets.background_tileset_texture,
                    texture_size: [256, 128],
                    tile_size: [256, 128],
                }
                .into_message(),
            );

            ExecutionResult::Processed
        } else if message.name == GameStarted::category_name() {
            self.active_tilemaps.iter().for_each(|(_, tilemap_id)| {
                _ = self.dispatcher.send(
                    DetachTileMap {
                        tilemap_id: *tilemap_id,
                    }
                    .into_message(),
                );
            });
            self.active_tilemaps.clear();
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(
            [
                AppTick::category_name().into(),
                GameReady::category_name().into(),
                GameStarted::category_name().into(),
            ]
            .into(),
        )
    }
}
