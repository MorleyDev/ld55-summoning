use std::sync::Arc;

use game_components::Lifespan;
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
pub struct EntityLifespanActor {
    pub ecs: Arc<dyn EcsRegistry>,
}
impl EntityLifespanActor {
    fn on_tick(&self, delta_time: f64) {
        // remove any entity that has ran out of lifespan
        self.ecs
            .query_choose(|id, lifespan: &mut Lifespan| {
                lifespan.0 -= delta_time;
                if lifespan.0 > 0.0 {
                    None
                } else {
                    Some(id)
                }
            })
            .into_iter()
            .for_each(|id| _ = self.ecs.destroy_entity(id));
    }
}
impl IsDispatcher for EntityLifespanActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == "app:tick" {
            if let Some(msg) = AppTick::from_message(message) {
                self.on_tick(msg.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
