use std::sync::Arc;

use game_components::SpriteFlash;
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::Sprite;

pub struct SpriteFlashActor {
    pub ecs: Arc<dyn EcsRegistry>,
}
impl SpriteFlashActor {
    fn on_tick(&self, delta_time: f64) {
        self.ecs
            .query_choose(|id, (sprite, flash): (&mut Sprite, &mut SpriteFlash)| {
                flash.timer -= delta_time;
                if flash.timer <= 0.0 {
                    sprite.color = [1.0, 1.0, 1.0, 1.0];
                    Some(id)
                } else {
                    let color = (flash.timer / flash.length) * 2.0;
                    if color <= 1.0 {
                        sprite.color = [1.0 - color as f32, 0.0, 0.0, 1.0];
                    } else {
                        sprite.color = [color as f32 - 1.0, 0.0, 0.0, 1.0];
                    }
                    None
                }
            })
            .into_iter()
            .for_each(|id| {
                _ = self.ecs.detach_component::<SpriteFlash>(id);
            })
    }
}
impl IsDispatcher for SpriteFlashActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            if let Some(message) = AppTick::from_message(message) {
                self.on_tick(message.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
