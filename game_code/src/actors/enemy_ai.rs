use game_components::{AiDisabled, AiPackageAdvanceTowardsPlayer, Player, Velocity};
use std::sync::Arc;
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{component_query::Without, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::Sprite;
use waef_world2d::GlobalTransform;

use crate::util::Vector2;

pub struct EnemyAiActor {
    pub ecs: Arc<dyn EcsRegistry>,
    pub ai_tick_cooldown: f64,
}
impl EnemyAiActor {
    fn on_tick(&mut self, delta_time: f64) {
        self.ecs
            .query_choose(|id, package: &mut AiDisabled| {
                package.lifetime -= delta_time;
                if package.lifetime <= 0.0 {
                    Some(id)
                } else {
                    None
                }
            })
            .into_iter()
            .for_each(|id| _ = self.ecs.detach_component::<AiDisabled>(id));

        self.ai_tick_cooldown -= delta_time;
        if self.ai_tick_cooldown <= 0.0 {
            self.ai_tick_cooldown += 0.1;

            if let Some(player_origin) = self
                .ecs
                .query_map(|_, (_, transform): (&Player, &GlobalTransform)| transform.origin)
                .get(0)
                .cloned()
            {
                self.ecs.query(
                    |_,
                     ((sprite, transform), (package, Velocity(velocity)), _): (
                        (&mut Sprite, &GlobalTransform),
                        (&AiPackageAdvanceTowardsPlayer, &mut Velocity),
                        Without<AiDisabled>,
                    )| {
                        let diff = [
                            player_origin[0] - transform.origin[0],
                            player_origin[1] - transform.origin[1],
                        ];
                        let delta = Vector2::mult(Vector2::normalize(diff), package.speed);
                        velocity[0] = delta[0];
                        velocity[1] = delta[1];

                        if velocity[0] < 0.0 {
                            sprite.texture_area.top = 0;
                        } else if velocity[0] > 0.0 {
                            sprite.texture_area.top = sprite.texture_area.height;
                        }
                    },
                )
            }
        }
    }
}
impl IsDispatcher for EnemyAiActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            if let Some(message) = AppTick::from_message(message) {
                self.on_tick(message.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> waef_core::core::DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
