use std::sync::{mpsc::Sender, Arc};

use game_components::{
    AnimatedSprite, Attack1Generator, Attack2PortalGenerator, Attack3Generator, GameEntity,
    GameWidget, Player,
};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{builders::EntityBuilder, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_graphics2d::ecs::{Sprite, TextureArea};
use waef_world2d::Transform;

use crate::{
    assets::GameAssets,
    messages::{GameOver, GameStarted, GameVictory},
};

pub enum VictoryCountdown {
    Inactive,
    Active((f64, i32)),
}

pub struct GamePhaseActor {
    pub countdown: VictoryCountdown,
    pub dispatcher: Sender<Message>,
    pub ecs: Arc<dyn EcsRegistry>,
    pub assets: GameAssets,
    pub total_game_length: f64,
}
impl GamePhaseActor {
    fn on_started(&mut self) {
        self.countdown = VictoryCountdown::Active((self.total_game_length, 0));
    }

    fn on_victory(&mut self) {
        self.countdown = VictoryCountdown::Inactive;
    }

    fn on_gameover(&mut self) {
        self.countdown = VictoryCountdown::Inactive;
    }

    fn on_tick(&mut self, delta_time: f64) {
        match self.countdown {
            VictoryCountdown::Inactive => (),
            VictoryCountdown::Active((timer, mut active_phase)) => {
                let countdown_ms = 0.0f64.max(timer - delta_time);
                if countdown_ms < 155.0 && active_phase == 0 {
                    self.ecs
                        .query_map(|id, _: &Player| id)
                        .into_iter()
                        .for_each(|id| {
                            println!("Enabling attack 2");
                            _ = self.ecs.attach_component(
                                id,
                                Attack2PortalGenerator {
                                    cooldown: 3.0,
                                    next_rotation: 0.0,
                                    period: 3.0
                                },
                            )
                        });

                    active_phase = active_phase + 1;
                } else if countdown_ms <= 120.0 && active_phase == 1 {
                    println!("Speeding up attack 1");
                    self.ecs
                        .query(|_, attack: &mut Attack1Generator| attack.period = 1.0);

                    println!("Phase 1 start");
                    _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                        .with_component(GameEntity {})
                        .with_component(GameWidget([0.0, 0.0]))
                        .with_component(Transform {
                            origin: [0.0, 0.0],
                            rotation_rads: 0.0,
                        })
                        .with_component(AnimatedSprite {
                            frames: 4,
                            length: 1.0,
                            loops: true,
                            time: 0.0,
                        })
                        .with_component(Sprite {
                            texture_area: TextureArea {
                                left: 0,
                                top: 0,
                                height: 256,
                                width: 256,
                            },
                            size: [32.0, 32.0],
                            texture_id: self.assets.victory_effect_1,
                            z_index: 50,
                            ..Default::default()
                        })
                        .create(&self.ecs);
                    active_phase = active_phase + 1;
                } else if countdown_ms <= 90.0 && active_phase == 2 {
                    self.ecs
                        .query_map(|id, _: &Player| id)
                        .into_iter()
                        .for_each(|id| {
                            println!("Enabling attack 3");
                            _ = self.ecs.attach_component(
                                id,
                                Attack3Generator {
                                    cooldown: 1.0,
                                    period: 1.0,
                                    direction: false,
                                    generate_both_sides: false
                                },
                            );
                        });

                    println!("Phase 2 start");
                    _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                        .with_component(GameEntity {})
                        .with_component(GameWidget([0.0, 0.0]))
                        .with_component(Transform {
                            origin: [0.0, 0.0],
                            rotation_rads: 0.0,
                        })
                        .with_component(AnimatedSprite {
                            frames: 4,
                            length: 2.0,
                            loops: true,
                            time: 0.0,
                        })
                        .with_component(Sprite {
                            texture_area: TextureArea {
                                left: 0,
                                top: 0,
                                height: 256,
                                width: 256,
                            },
                            size: [16.0, 16.0],
                            texture_id: self.assets.victory_effect_2,
                            z_index: 51,
                            ..Default::default()
                        })
                        .create(&self.ecs);
                    active_phase = active_phase + 1;
                } else if countdown_ms <= 60.0 && active_phase == 3 {
                    println!("Phase 3 start");
                    _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                        .with_component(GameEntity {})
                        .with_component(GameWidget([0.0, 0.0]))
                        .with_component(Transform {
                            origin: [0.0, 0.0],
                            rotation_rads: 0.0,
                        })
                        .with_component(AnimatedSprite {
                            frames: 4,
                            length: 0.5,
                            loops: true,
                            time: 0.0,
                        })
                        .with_component(Sprite {
                            texture_area: TextureArea {
                                left: 0,
                                top: 0,
                                height: 256,
                                width: 256,
                            },
                            size: [16.0, 16.0],
                            texture_id: self.assets.victory_effect_3,
                            z_index: 52,
                            ..Default::default()
                        })
                        .create(&self.ecs);
                    active_phase = active_phase + 1;
                } else if countdown_ms <= 30.0 && active_phase == 4 {
                    println!("Speeding up attack 2");
                    self.ecs
                        .query(|_, attack: &mut Attack2PortalGenerator| attack.period = 2.5);

                    println!("Phase 4 start");

                    _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                        .with_component(GameEntity {})
                        .with_component(GameWidget([0.0, 0.0]))
                        .with_component(Transform {
                            origin: [0.0, 0.0],
                            rotation_rads: 0.0,
                        })
                        .with_component(AnimatedSprite {
                            frames: 4,
                            length: 1.2,
                            loops: true,
                            time: 0.0,
                        })
                        .with_component(Sprite {
                            texture_area: TextureArea {
                                left: 0,
                                top: 0,
                                height: 256,
                                width: 256,
                            },
                            size: [16.0, 16.0],
                            texture_id: self.assets.victory_effect_4,
                            z_index: 53,
                            ..Default::default()
                        })
                        .create(&self.ecs);
                    active_phase = active_phase + 1;
                } else if countdown_ms <= 60.0 && active_phase == 5 {
                    println!("Speeding up attack 1 & 3");
                    self.ecs
                        .query(|_, attack: &mut Attack1Generator| attack.generate_both_sides = true);
                    self.ecs
                        .query(|_, attack: &mut Attack3Generator| attack.generate_both_sides = true);

                    println!("Phase 5 start");

                    _ = EntityBuilder::new(uuid::Uuid::new_v4().as_u128())
                    .with_component(GameEntity {})
                        .with_component(GameWidget([0.0, 0.0]))
                        .with_component(Transform {
                            origin: [0.0, 0.0],
                            rotation_rads: 0.0,
                        })
                        .with_component(AnimatedSprite {
                            frames: 4,
                            length: 0.2,
                            loops: true,
                            time: 0.0,
                        })
                        .with_component(Sprite {
                            texture_area: TextureArea {
                                left: 0,
                                top: 0,
                                height: 256,
                                width: 256,
                            },
                            size: [2.0, 2.0],
                            texture_id: self.assets.victory_effect_5,
                            z_index: 54,
                            ..Default::default()
                        })
                        .create(&self.ecs);
                    active_phase = active_phase + 1;
                } else if countdown_ms <= 0.0 && active_phase == 6 {
                    active_phase = active_phase + 1;
                    _ = self.dispatcher.send(GameVictory {}.into_message());
                }
                self.countdown = VictoryCountdown::Active((countdown_ms, active_phase));
            }
        }
    }
}
impl IsDispatcher for GamePhaseActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            if let Some(tick) = AppTick::from_message(message) {
                self.on_tick(tick.delta_time);
            }
            ExecutionResult::Processed
        } else if message.name == GameStarted::category_name() {
            self.on_started();
            ExecutionResult::Processed
        } else if message.name == GameOver::category_name() {
            self.on_gameover();
            ExecutionResult::Processed
        } else if message.name == GameVictory::category_name() {
            self.on_victory();
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(
            [
                AppTick::category_name().into(),
                GameOver::category_name().into(),
                GameStarted::category_name().into(),
                GameVictory::category_name().into(),
            ]
            .into(),
        )
    }
}
