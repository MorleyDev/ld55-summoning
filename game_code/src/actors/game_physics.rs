use std::sync::Arc;

use game_components::{CollisionSensor, CollisionSensorTween, Velocity};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{component_query::Without, extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_world2d::{
    actor::{apply_parented_global_transforms, apply_parentless_global_transforms},
    GlobalTransform, Parent, Transform,
};

use crate::util::Vector2;

pub struct GamePhysicsActor {
    pub ecs: Arc<dyn EcsRegistry>,
}
impl GamePhysicsActor {
    fn on_tick(&self, delta_time: f64) {
        self.ecs
            .query_map(
                |id, (transform, _, _): (&Transform, Without<GlobalTransform>, Without<Parent>)| {
                    (
                        id,
                        GlobalTransform {
                            origin: transform.origin,
                            rotation_rads: transform.rotation_rads,
                        },
                    )
                },
            )
            .into_iter()
            .for_each(|(id, global_transform)| {
                _ = self.ecs.attach_component(id, global_transform);
            });

            self.ecs
            .query_choose(
                |id, (transform, _, parent): (&Transform, Without<GlobalTransform>, &Parent)| {
                    let mut result = None;
                    self.ecs.query_single(parent.entity_id, |_, parent_transform: &GlobalTransform| {
                        result = Some(GlobalTransform {
                            origin: Vector2::add(parent_transform.origin, transform.origin),
                            rotation_rads: parent_transform.rotation_rads + transform.rotation_rads
                        });
                    });
                    result.map(|global_transform| (id, global_transform))
                },
            )
            .into_iter()
            .for_each(|(id, global_transform)| {
                _ = self.ecs.attach_component(id, global_transform);
            });

        // apply any velocity transformations to entities
        self.ecs.query(
            |_id, (transform, Velocity(velocity)): (&mut Transform, &Velocity)| {
                transform.origin[0] += velocity[0] * delta_time as f32;
                transform.origin[1] += velocity[1] * delta_time as f32;
            },
        );

        // apply the collision sensor tweening
        self.ecs
            .query_choose(
                |id, (sensor, tween): (&mut CollisionSensor, &mut CollisionSensorTween)| {
                    tween.timer += delta_time;
                    if tween.timer >= tween.length {
                        sensor.size = tween.target_size;
                        sensor.origin = tween.target_origin;
                        return Some(id);
                    } else {
                        let delta_factor = (tween.timer / tween.length) as f32;

                        let tween_size_diff = [
                            tween.target_size[0] - tween.initial_size[0],
                            tween.target_size[1] - tween.initial_size[1],
                        ];
                        sensor.size = [
                            tween.initial_size[0] + tween_size_diff[0] * delta_factor,
                            tween.initial_size[1] + tween_size_diff[1] * delta_factor,
                        ];

                        let tween_origin_diff = [
                            tween.target_origin[0] - tween.initial_origin[0],
                            tween.target_origin[1] - tween.initial_origin[1]
                        ];
                        sensor.origin = [
                            tween.initial_origin[0] + tween_origin_diff[0] * delta_factor,
                            tween.initial_origin[1] + tween_origin_diff[1] * delta_factor,
                        ];

                        None
                    }
                },
            )
            .into_iter()
            .for_each(|id| _ = self.ecs.detach_component::<CollisionSensorTween>(id));

        self.ecs.query(apply_parentless_global_transforms);
        apply_parented_global_transforms(&self.ecs);
    }
}
impl IsDispatcher for GamePhysicsActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == "app:tick" {
            if let Some(msg) = AppTick::from_message(message) {
                self.on_tick(msg.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }
}
