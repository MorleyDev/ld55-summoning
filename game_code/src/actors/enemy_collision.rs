use std::{
    collections::HashSet,
    sync::{mpsc::Sender, Arc},
};

use game_components::{CollisionSensor, Enemy, Player, SpriteFlash};
use waef_core::{
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsMessage, Message},
    ecs::{extensions::EcsExtension, EcsRegistry},
    timers::messages::AppTick,
};
use waef_kira::messages::PlaySound;
use waef_world2d::GlobalTransform;

use crate::{assets::GameAssets, util::Geometry};

pub struct PlayerDamageActor {
    pub ecs: Arc<dyn EcsRegistry>,
    pub dispatcher: Sender<Message>,
    pub assets: GameAssets,
}
impl PlayerDamageActor {
    fn on_tick(&self, delta_time: f64) {
        let mut collisions = HashSet::with_capacity(32);
        self.ecs.query(
            |lhs,
             (lhs_player, lhs_transform, lhs_sensor): (
                &mut Player,
                &GlobalTransform,
                &CollisionSensor,
            )| {
                if lhs_player.damage_cooldown > 0.0 {
                    lhs_player.damage_cooldown =
                        0.0f64.max(lhs_player.damage_cooldown - delta_time);
                    return;
                }

                let lhs_hsize = [lhs_sensor.size[0] / 2.0, lhs_sensor.size[1] / 2.0];
                let lhs_origin = [
                    lhs_sensor.origin[0] + lhs_transform.origin[0],
                    lhs_sensor.origin[1] + lhs_transform.origin[1],
                ];
                let lhs_tlbr = [
                    lhs_origin[0] - lhs_hsize[0],
                    lhs_origin[1] - lhs_hsize[1],
                    lhs_origin[0] + lhs_hsize[0],
                    lhs_origin[1] + lhs_hsize[1],
                ];

                self.ecs.query(
                    |rhs,
                     (rhs_enemy, rhs_transform, rhs_sensor): (
                        &Enemy,
                        &GlobalTransform,
                        &CollisionSensor,
                    )| {
                        if lhs_player.damage_cooldown > 0.0 {
                            return;
                        }

                        if lhs != rhs {
                            let rhs_hsize = [rhs_sensor.size[0] / 2.0, rhs_sensor.size[1] / 2.0];
                            let rhs_origin = [
                                rhs_sensor.origin[0] + rhs_transform.origin[0],
                                rhs_sensor.origin[1] + rhs_transform.origin[1],
                            ];
                            let rhs_tlbr = [
                                rhs_origin[0] - rhs_hsize[0],
                                rhs_origin[1] - rhs_hsize[1],
                                rhs_origin[0] + rhs_hsize[0],
                                rhs_origin[1] + rhs_hsize[1],
                            ];

                            if Geometry::aabb(lhs_tlbr, rhs_tlbr) {
                                lhs_player.damage_cooldown = 1.0;
                                lhs_player.health =
                                    0.0f32.max(lhs_player.health - rhs_enemy.attack_power);
                                collisions.insert(lhs);

                                if lhs_player.health > 0.0 {
                                    _ = self.dispatcher.send(
                                        PlaySound {
                                            loops: false,
                                            sound_buffer_id: self.assets.player_hit_sound,
                                            sound_id: uuid::Uuid::new_v4().as_u128(),
                                            volume: 0.2,
                                        }
                                        .into_message(),
                                    );
                                }
                            }
                        }
                    },
                );
            },
        );
        collisions.into_iter().for_each(|player| {
            _ = self.ecs.attach_component(
                player,
                SpriteFlash {
                    length: 1.0,
                    timer: 1.0,
                },
            );
        })
    }
}
impl IsDispatcher for PlayerDamageActor {
    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([AppTick::category_name().into()].into())
    }

    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == AppTick::category_name() {
            if let Some(message) = AppTick::from_message(message) {
                self.on_tick(message.delta_time);
            }
            ExecutionResult::Processed
        } else {
            ExecutionResult::NoOp
        }
    }
}