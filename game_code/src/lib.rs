mod actors;
mod assets;
mod messages;
mod util;

use std::{
    collections::HashMap,
    sync::{mpsc::Sender, Arc},
};

use actors::{
    attack_collision::AttackCollision,
    enemy_ai::EnemyAiActor,
    enemy_collision::PlayerDamageActor,
    enemy_spawning::{EnemySpawningActor, SpawningLogic},
    entity_lifespan::EntityLifespanActor,
    game_physics::GamePhysicsActor,
    game_state::{GameState, GameStateActor},
    phase::{GamePhaseActor, VictoryCountdown},
    player_attacks::PlayerAttacksActor,
    player_health::PlayerHealthActor,
    sprite_animation::GameAnimationActor,
    sprite_flash::SpriteFlashActor,
    tilemap_generation::TilemapGenerationActor,
};
use messages::GameReady;
use waef_assetloader::AssetLoader;
use waef_core::{
    actors::ActorsPlugin, ActorAlignment, DispatchFilter, EcsRegistry, ExecutionResult,
    HasActorAlignment, IsActor, IsDispatcher, IsDisposable, Message,
};

struct ActorAsDispatcher {
    weight: u32,
    dispatcher: Box<dyn IsDispatcher + Send>,
}
impl IsDispatcher for ActorAsDispatcher {
    fn filter(&self) -> DispatchFilter {
        self.dispatcher.filter()
    }

    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if self.dispatcher.filter().allows(message) {
            self.dispatcher.dispatch(message)
        } else {
            ExecutionResult::NoOp
        }
    }
}
impl IsDisposable for ActorAsDispatcher {
    fn dispose(&mut self) {}
}
impl HasActorAlignment for ActorAsDispatcher {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}
impl IsActor for ActorAsDispatcher {
    fn weight(&self) -> u32 {
        self.weight
    }

    fn name(&self) -> String {
        "game_code".to_string()
    }
}

struct GameCodePlugin {
    pub dispatchers: Vec<(u32, Box<dyn IsDispatcher + Send>)>,
}
impl GameCodePlugin {
    pub fn new() -> Self {
        Self {
            dispatchers: vec![],
        }
    }
    pub fn use_dispatcher(
        mut self,
        weight: u32,
        dispatcher: impl IsDispatcher + Send + 'static,
    ) -> Self {
        self.dispatchers.push((weight, Box::new(dispatcher)));
        self
    }
}
impl ActorsPlugin for GameCodePlugin {
    fn apply<T: waef_core::actors::UseActors>(self, target: T) -> T {
        self.dispatchers
            .into_iter()
            .fold(target, |target, (weight, dispatcher)| {
                target.use_actor(Box::new(ActorAsDispatcher { dispatcher, weight }))
            })
    }
}

pub fn create_game_code_plugin(
    ecs: Arc<dyn EcsRegistry>,
    dispatcher: Sender<Message>,
) -> impl ActorsPlugin + 'static {
    let asset_loader = AssetLoader::new(dispatcher.clone());
    let assets = assets::GameAssets::open(&asset_loader, GameReady {});

    GameCodePlugin::new()
        .use_dispatcher(
            2,
            GameStateActor {
                ecs: ecs.clone(),
                dispatcher: dispatcher.clone(),
                assets: assets.clone(),
                game_state: GameState::Waiting,
            },
        )
        .use_dispatcher(1, GameAnimationActor { ecs: ecs.clone() })
        .use_dispatcher(1, EntityLifespanActor { ecs: ecs.clone() })
        .use_dispatcher(
            4,
            EnemySpawningActor {
                ecs: ecs.clone(),
                assets: assets.clone(),
                state: SpawningLogic::Inactive,
                tick_count: 0,
                total_game_length: 180.0,
            },
        )
        .use_dispatcher(
            16,
            EnemyAiActor {
                ecs: ecs.clone(),
                ai_tick_cooldown: 0.1,
            },
        )
        .use_dispatcher(
            16,
            AttackCollision {
                ecs: ecs.clone(),
                assets: assets.clone(),
                dispatcher: dispatcher.clone(),
            },
        )
        .use_dispatcher(
            4,
            PlayerAttacksActor {
                ecs: ecs.clone(),
                dispatcher: dispatcher.clone(),
                assets: assets.clone(),
                tick_count: 0,
            },
        )
        .use_dispatcher(
            8,
            PlayerDamageActor {
                ecs: ecs.clone(),
                assets: assets.clone(),
                dispatcher: dispatcher.clone(),
            },
        )
        .use_dispatcher(
            1,
            PlayerHealthActor {
                ecs: ecs.clone(),
                assets: assets.clone(),
                dispatcher: dispatcher.clone(),
            },
        )
        .use_dispatcher(
            2,
            TilemapGenerationActor {
                active_tilemaps: HashMap::with_capacity(9),
                assets: assets.clone(),
                dispatcher: dispatcher.clone(),
                ecs: ecs.clone(),
            },
        )
        .use_dispatcher(
            4,
            GamePhaseActor {
                countdown: VictoryCountdown::Inactive,
                dispatcher: dispatcher.clone(),
                ecs: ecs.clone(),
                assets: assets.clone(),
                total_game_length: 180.0,
            },
        )
        .use_dispatcher(4, SpriteFlashActor { ecs: ecs.clone() })
        .use_dispatcher(32, GamePhysicsActor { ecs: ecs.clone() })
}
