use waef_assetloader::{AssetBundleBuilder, AssetLoader};
use waef_core::core::IsMessage;

#[derive(Clone)]
pub struct GameAssets {
    pub title_texture: u128,
    pub victory_texture: u128,

    pub player_texture: u128,
    pub enemy_1_texture: u128,
    pub healthbar_overlay_texture: u128,
    pub healthbar_underlay_texture: u128,
    pub background_tileset_texture: u128,
    pub background_tileset: u128,
    pub attack_1_texture: u128,
    pub attack_2_portal_texture: u128,
    pub attack_2_texture: u128,
    pub attack_3_texture: u128,

    pub attack_1_sound: u128,
    pub enemy_1_hit_sound: u128,
    pub enemy_1_death_sound: u128,
    pub player_hit_sound: u128,
    pub player_death_sound: u128,
    pub victory_sound: u128,

    pub title_music: u128,
    pub game_music: u128,
    pub victory_music: u128,
    pub gameover_music: u128,

    pub victory_effect_1: u128,
    pub victory_effect_2: u128,
    pub victory_effect_3: u128,
    pub victory_effect_4: u128,
    pub victory_effect_5: u128,
}

impl GameAssets {
    pub fn open(asset_loader: &AssetLoader, and_then: impl IsMessage) -> GameAssets {
        let mut game_assets_bundle = AssetBundleBuilder::new();

        let assets = GameAssets {
            title_texture: game_assets_bundle.add_asset("texture", "./title_screen.png"),
            victory_texture: game_assets_bundle.add_asset("texture", "./victory.png"),

            healthbar_overlay_texture: game_assets_bundle
                .add_asset("texture", "./healthbar_overlay.png"),
            healthbar_underlay_texture: game_assets_bundle
                .add_asset("texture", "./healthbar_underlay.png"),

            background_tileset_texture: game_assets_bundle.add_asset("texture", "./background.png"),
            background_tileset: uuid::Uuid::new_v4().as_u128(),

            player_texture: game_assets_bundle.add_asset("texture", "./player.png"),
            enemy_1_texture: game_assets_bundle.add_asset("texture", "./enemy_1.png"),
            attack_1_texture: game_assets_bundle.add_asset("texture", "./player_attack_1.png"),
            attack_2_portal_texture: game_assets_bundle
                .add_asset("texture", "./attack_2_portal.png"),
            attack_2_texture: game_assets_bundle.add_asset("texture", "./attack_2.png"),
            attack_3_texture: game_assets_bundle.add_asset("texture", "./attack_3.png"),

            title_music: game_assets_bundle.add_asset("sound_buffer", "./title_screen.ogg"),
            game_music: game_assets_bundle.add_asset("sound_buffer", "./gameplay.ogg"),
            gameover_music: game_assets_bundle.add_asset("sound_buffer", "./gameover.ogg"),
            victory_music: game_assets_bundle.add_asset("sound_buffer", "./victory.ogg"),

            player_hit_sound: game_assets_bundle.add_asset("sound_buffer", "./player_hit.wav"),
            player_death_sound: game_assets_bundle.add_asset("sound_buffer", "./player_death.wav"),

            attack_1_sound: game_assets_bundle.add_asset("sound_buffer", "./attack_1_sound.wav"),

            enemy_1_hit_sound: game_assets_bundle.add_asset("sound_buffer", "./enemy_1_hit.wav"),
            enemy_1_death_sound: game_assets_bundle
                .add_asset("sound_buffer", "./enemy_1_death.wav"),
            victory_sound: game_assets_bundle.add_asset("sound_buffer", "./victory.wav"),

            victory_effect_1: game_assets_bundle.add_asset("texture", "./victory_phase_1.png"),
            victory_effect_2: game_assets_bundle.add_asset("texture", "./victory_phase_2.png"),
            victory_effect_3: game_assets_bundle.add_asset("texture", "./victory_phase_3.png"),
            victory_effect_4: game_assets_bundle.add_asset("texture", "./victory_phase_4.png"),
            victory_effect_5: game_assets_bundle.add_asset("texture", "./victory_phase_5.png"),
        };
        game_assets_bundle.and_then(and_then);

        asset_loader.open_bundle(game_assets_bundle);
        assets
    }
}
