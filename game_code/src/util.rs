use std::num::Wrapping;

pub struct Geometry {}
impl Geometry {
    // lhs: (left, top, bottom, right)
    // rhs: (left, top, bottom, right)
    pub fn aabb(lhs: [f32; 4], rhs: [f32; 4]) -> bool {
        !(lhs[0] >= rhs[2] || lhs[1] >= rhs[3] || lhs[2] < rhs[0] || lhs[3] < rhs[1])
    }
}

pub struct Vector2 {}
impl Vector2 {
    pub fn magnitude_squared([x, y]: [f32; 2]) -> f32 {
        x * x + y * y
    }

    pub fn magnitude(xy: [f32; 2]) -> f32 {
        Self::magnitude_squared(xy).sqrt()
    }
    pub fn normalize([x, y]: [f32; 2]) -> [f32; 2] {
        let magnitude = Self::magnitude([x, y]);
        if magnitude == 0.0 {
            [1.0 / 2.0f32.sqrt(), 1.0 / 2.0f32.sqrt()]
        } else {
            [x / magnitude, y / magnitude]
        }
    }

    pub fn mult([x, y]: [f32; 2], f: f32) -> [f32; 2] {
        [x * f, y * f]
    }
    
    pub fn add([x1, y1]: [f32; 2], [x2, y2]: [f32; 2]) -> [f32; 2] {
        [x1 + x2, y1 + y2]
    }
}

pub struct Noise {}
impl Noise {
    #![allow(arithmetic_overflow)]

    pub fn noise_u32(seed: u32, index: u32) -> u32 {
        let bit_noise1 = 0xb5297a4du32;
        let bit_noise2 = 0x68e31da4u32;
        let bit_noise3 = 0x1b56c4e9u32;

        let mut mangled = Wrapping(index);
        mangled *= bit_noise1;
        mangled += seed;
        mangled ^= mangled >> 8;
        mangled += bit_noise2;
        mangled ^= mangled << 8;
        mangled *= bit_noise3;
        mangled ^= mangled >> 8;
        mangled.0
    }

    pub fn noise_u16(seed: u32, index: u32) -> u16 {
        ((Self::noise_u32(seed, index) & 0x00ffff00u32) >> 8) as u16
    }

    // range [-1, 1]
    pub fn noise_f32(seed: u32, index: u32) -> f32 {
        (Self::noise_u16(seed, index) as f32) / 32767.5 - 1.0
    }
}
