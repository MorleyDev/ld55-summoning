use waef_core::{Pod, message_pod};

#[derive(Clone, Copy, Pod)]
#[message_pod("game:ready")]
pub struct GameReady {}

#[derive(Clone, Copy, Pod)]
#[message_pod("game:gamestarted")]
pub struct GameStarted {}

#[derive(Clone, Copy, Pod)]
#[message_pod("game:gameover")]
pub struct GameOver {}

#[derive(Clone, Copy, Pod)]
#[message_pod("game:gamevictory")]
pub struct GameVictory {}
