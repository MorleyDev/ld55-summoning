use std::sync::Arc;

use game_code::create_game_code_plugin;
use game_components::define_game_code_components;
use waef_core::{ecs::EventEmittingEcsRegistry, EcsRegistry, WaefError};
use waef_ecs::WaefEcsRegistry;
use waef_runtime::{run, DefaultBuilder};

pub struct TracingEcsRegistry {
    pub parent: Box<dyn EcsRegistry>
}
impl TracingEcsRegistry {
    pub fn new_arc(parent: Box<dyn EcsRegistry>) -> Arc<dyn EcsRegistry> {
        Arc::new(Self { parent })
    }
}
impl EcsRegistry for TracingEcsRegistry {
    fn list_component_definitions(&self) -> std::collections::HashMap<String, usize> {
        let _guard = tracing::info!("ecs:list_component_definitions");
        self.parent.list_component_definitions()
    }

    fn define_component_by_size(&self, name: String, size: usize) -> Result<(), WaefError> {
        let _guard = tracing::info!("ecs:define_component_by_size");
        self.parent.define_component_by_size(name, size)
    }

    fn get_component_size(&self, name: String) -> Option<usize> {
        let _guard = tracing::info!("ecs:get_component_size");
        self.parent.get_component_size(name)
    }

    fn create_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        let _guard = tracing::info!("ecs:create_entity");
        self.parent.create_entity(entity_id)
    }

    fn create_entity_with_components(
            &self,
            entity_id: u128,
            components: std::collections::HashMap<String, Box<[u8]>>,
        ) -> Result<(), WaefError> {
        let _guard = tracing::info!("ecs:create_entity_with_components:{:?}", components.keys());
        self.parent.create_entity_with_components(entity_id, components)
    }

    fn destroy_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        let _guard = tracing::info!("ecs:destroy_entity");
        self.parent.destroy_entity(entity_id)
    }

    fn attach_component_buffer(
        &self,
        component_name: String,
        entity_id: u128,
        component_buffer: Box<[u8]>,
    ) -> Result<(), WaefError> {
        let _guard = tracing::info!("ecs:attach_component_buffer:{}", component_name);
        self.parent.attach_component_buffer(component_name, entity_id, component_buffer)
    }

    fn detach_component_by_name(
        &self,
        component_name: String,
        entity_id: u128,
    ) -> Result<(), WaefError> {
        let _guard = tracing::info!("ecs:detach_component_by_name");
        self.parent.detach_component_by_name(component_name, entity_id)
    }

    fn apply(&self, component_query: &[String], callback: &mut dyn FnMut(u128, &mut [&mut [u8]])) {
        let _guard = tracing::info!("ecs:apply");
        self.parent.apply(component_query, callback)
    }

    fn apply_set(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let _guard = tracing::info!("ecs:apply_set:{:?}", component_query);
        self.parent.apply_set(entity_ids, component_query, callback)
    }

    fn apply_single(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let _guard = tracing::info!("ecs:apply_single:{:?}", component_query);
        self.parent.apply_single(entity_id, component_query, callback)
    }

    fn apply_set_unsafe(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::info!("ecs:apply_set_unsafe:{:?}", component_query);
        self.parent.apply_set_unsafe(entity_ids, component_query, callback)
    }

    fn apply_unsafe(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::info!("ecs:apply_unsafe:{:?}", component_query);
        self.parent.apply_unsafe(component_query, callback)
    }

    fn apply_single_unsafe(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::info!("ecs:apply_single_unsafe:{:?}", component_query);
        self.parent.apply_single_unsafe(entity_id, component_query, callback)
    }
}

pub fn main() -> Result<(), WaefError> {
    #[cfg(feature = "js")]
    console_error_panic_hook::set_once();

    run(|cfg| {
        let builder = DefaultBuilder::new(cfg).configure_ecs(|b| {
            EventEmittingEcsRegistry::new_arc(b.dispatcher(), Box::new(WaefEcsRegistry::new()))
        });

        define_game_code_components(&builder.ecs());

        builder
            .configure_actors(|builder, actors| {
                actors.use_plugin(create_game_code_plugin(builder.ecs(), builder.dispatcher()))
            })
            .run()
    })
}
